vcl 4.0;

backend default {
    .host = "fuseki";
    .port = "3030";
    .first_byte_timeout = 600s;
}

# Note: Currently we just re-create the container to drop the whole cache,
#       but we might re-enable something like this in the future for more
#       fine-grained cache invalidation control.
# acl banners {
#     "172.20.0.1";  # Docker host (see docker-compose.yml)
# }
#
# sub vcl_recv {
#     # req: The request as soon as it arrive at Varnish
#     if (req.method == "BAN") {
#         if (!client.ip ~ banners) {
#             return(synth(405, "Not allowed from this IP."));
#         }
#
#         # Lurker-friendly ban
#         # Assumes the ``X-Ban-Url`` header is a regex,
#         # this might be a bit too simple.
#         # <http://book.varnish-software.com/4.0/chapters/Cache_Invalidation.html>
#         ban("obj.http.x-url ~ " + req.http.x-ban-url);
#
#         # Throw a synthetic page so the request won't go to the backend.
#         return(synth(200, "Ban added"));
#     }
# }

sub vcl_backend_response {
    # bereq: The request that goes to the backend
    # beresp: The backend response

    # An object lives in cache until TTL + grace + keep elapses.

    # beresp.ttl is initialized with a value for the following status codes:
    # 200,203,300,301,302,307,404,410
    # Ref: http://book.varnish-software.com/4.0/chapters/VCL_Basics.html#the-initial-value-of-beresp-ttl
    # By checking if beresp.ttl already have a value, we avoid caching
    # stuff with other status code (like 500) without checking the status code explicitly.
    if (beresp.ttl > 0s) {
        # Store objects for a long time by default (1 week)
        set beresp.ttl = 1w;

        # Allow Varnish to also serve stale objects that are up to five minutes out of date.
        # When it does it will also schedule a refresh of the object.
        set beresp.grace = 5m;

        # Unset cookies so we can cache more requests.
        # Safe since we are not using sessions.
        unset beresp.http.set-cookie;

        /* Set header to notify the client that the response can be cached */
        set beresp.http.Cache-Control = "public, max-age=86400";

        # Always gzip before storing, to save space in the cache
        set beresp.do_gzip = true;

        # Part of cache invalidation
        set beresp.http.x-url = bereq.url;
    }

}

sub vcl_deliver {
    # resp: The final response that we will deliver to the client

    unset resp.http.Pragma;

    # Add a header indicating hit/miss
    if (obj.hits > 0) {
        set resp.http.X-Cache = "HIT";
    } else {
        set resp.http.X-Cache = "MISS";
    }

    # The X-Url header is for internal use only
    unset resp.http.x-url;
}
