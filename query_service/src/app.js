import dedent from "dedent";
import examples from './examples';

// -----------------------------------------------------------------------------

const defaultValue = `PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX bibbi: <https://id.bs.no/>
PREFIX bs: <https://schema.bs.no/>

SELECT ?entity ?entityLabel WHERE {
  ?entity a bs:Place ;
          skos:prefLabel ?entityLabel .

  FILTER(LANG(?entityLabel) = 'nb')
}

ORDER BY ASC(?entityLabel)
LIMIT 10`


// -----------------------------------------------------------------------------
// Add prefix autocompleter

const prefixList = [
  // Vocabularies we use
  'skos: <http://www.w3.org/2004/02/skos/core#>',
  'isothes: <http://purl.org/iso25964/skos-thes#>',
  'rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>',
  'rdfs: <http://www.w3.org/2000/01/rdf-schema#>',
  'dct: <http://purl.org/dc/terms/>',

  // Vocabularies for linked entities
  'wd: <http://www.wikidata.org/entity/>',
  'wdt: <http://www.wikidata.org/prop/direct/>',

  // Our own vocabulary
  'bibbi: <https://id.bs.no/>',
  'bs: <https://schema.bs.no/>',
];

YASQE.Autocompleters._prefixes = YASQE.Autocompleters.prefixes;
YASQE.Autocompleters.prefixes = function (yasqe, completerName) {
  var completer = YASQE.Autocompleters._prefixes(yasqe, completerName);
  completer.async = false;
  completer.get = prefixList;
  return completer;
};

// -----------------------------------------------------------------------------
// Add class autocompleter

YASQE.Autocompleters._classes = YASQE.Autocompleters.classes;
YASQE.Autocompleters.classes = function (yasqe, completerName) {
  var completer = YASQE.Autocompleters._classes(yasqe, completerName);
  let _get = completer.get.bind(completer);
  completer.get = function (token, callback) {
    if (!token || !token.string || token.string.trim().length == 0) {
      yasqe.autocompleters.notifications.getEl(completer).empty().append("Nothing to autocomplete yet!");
      return false;
    }

    if (token.autocompletionString.startsWith('https://schema.bs.no/')) {
      callback([
        "https://schema.bs.no/Topic",
        "https://schema.bs.no/Entity",
        "https://schema.bs.no/EntityCandidate",
        "https://schema.bs.no/Place",
        "https://schema.bs.no/Corporation",
        "https://schema.bs.no/Qualifier",
      ])
      return;
    }

    _get(token, function(res, n2) {
      console.log('Got results', res)
      callback(res);
    });
  };
  return completer;
};

// -----------------------------------------------------------------------------

var yasqe = YASQE(document.getElementById('yasqe'), {
  value: defaultValue,
  sparql: {
    showQueryButton: true,
    endpoint: '/query',
    requestMethod: "POST"
    /*defaultGraphs: ['http://data.ub.uio.no/rt']*/
  },
  viewportMargin: Infinity,
});

var yasr = YASR(document.getElementById('yasr'), {
  getUsedPrefixes: yasqe.getPrefixesFromQuery,
});

//link both together
yasqe.options.sparql.callbacks.complete = yasr.setResponse;

/*if (!yasr.somethingDrawn()) {
  yasqe.query();
}*/


examples.forEach(example => {
  $('#examples').append(`<option value="${example.id}">${example.title}</option>`);
})

$('#examples').on('change', function () {
  console.log(this.value);
  if (this.value == '') return;
  const example = examples.filter(ex => ex.id == this.value);
  if (example.length) {
    yasqe.setValue(dedent(example[0].query));
  }
});
