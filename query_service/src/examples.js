
export default [
  {
    id: 'example-1',
    title: 'Oversikt over datasett (grafer)',
    query: `
      PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

      SELECT
        ?graph
        ?tripleCount
        ?conceptCount
      WHERE
      {
        {
          SELECT ?graph (COUNT(?s) AS ?tripleCount)
          WHERE {
            GRAPH ?graph { ?s ?p ?o }
          }
          GROUP BY ?graph
        }
        OPTIONAL
        {
          SELECT ?graph (COUNT(?c) AS ?conceptCount)
          WHERE {
            GRAPH ?graph { ?c a skos:Concept }
          }
          GROUP BY ?graph
        }
      }
    `
  },
  {
    id: 'example-2',
    title: 'Bibbi-autoriteter etter bruksfrekvens',
    query: `
      PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
      PREFIX bs: <https://schema.bs.no/>
      PREFIX bibbi: <https://id.bs.no/bibbi/>
      
      SELECT
        ?concept
        ?conceptLabel
        ?itemCount
      WHERE
      {
        ?concept skos:inScheme bibbi: ;
                  bs:itemsAsSubject ?itemCount ;
                  skos:prefLabel ?conceptLabel .
        FILTER(lang(?conceptLabel) = "nb")
      }
      ORDER BY DESC(?itemCount)
    `
  },
  {
    id: 'example-3',
    title: 'WebDewey-numre etter antall Bibbi-autoriteter',
    query: `
      PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
      PREFIX bs: <https://schema.bs.no/>
      PREFIX bibbi: <https://id.bs.no/bibbi/>
      
      SELECT
        ?webDeweyNr
        ?webDeweyClass
        ?webDeweyHeading
        (COUNT(?concept) AS ?conceptCount)
      WHERE
      {
        ?concept skos:inScheme bibbi: ;
                bs:webdewey ?webDeweyNrRaw .
      
        # Fjerner /
        BIND(REPLACE(?webDeweyNrRaw, "/", "") AS ?webDeweyNr)
      
        OPTIONAL {
          ?webDeweyClass skos:notation ?webDeweyNr ;
                        skos:prefLabel ?webDeweyHeading .
        }
      }
      GROUP BY ?webDeweyNr ?webDeweyClass ?webDeweyHeading
      ORDER BY DESC(?conceptCount)
    `
  },
  {
    id: 'example-4',
    title: 'Bibbi-autoriteter med godkjente WebDewey-numre som ikke lar seg slå opp i Norsk WebDewey',
    query: `
      PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
      PREFIX bs: <https://schema.bs.no/>
      PREFIX bibbi: <https://id.bs.no/bibbi/>
      
      SELECT
        ?concept
        ?term
        ?itemCount
        ?webDeweyNumber
      WHERE
      {
      
        # Vi ønsker begreper som hører til vokabularet <https://id.bs.no/bibbi/> (Bibbi):
        ?concept skos:inScheme bibbi: .
      
        # I tillegg til URI-en ønsker vi foretrukket term (skos:prefLabel) og antall dokumenter (bs:itemCount)
        ?concept skos:prefLabel ?term .
        FILTER(LANG(?term) = "nb") # på bokmål
        ?concept bs:itemsAsSubject ?itemCount .
      
        # Vi ønsker bare begreper som har et (godkjent) WebDewey-nummer.
        # Fra egenskapen bs:webdewey får vi ut WebDewey-nummeret som streng, slik det står på posten:
        ?concept bs:webdewey ?webDeweyNumber .
      
        # Til slutt filtrerer vi på WebDewey-numre som *ikke* lar seg slå opp direkte i WebDewey
        # (typisk bygde numre som ikke er bygd i WebDewey)
        OPTIONAL {
          ?concept skos:closeMatch ?webDeweyClass .
          FILTER(STRSTARTS(STR(?webDeweyClass), "http://dewey.info"))
          ?webDeweyClass skos:inScheme <http://dewey.info/scheme/edition/e23/> .
        }
        FILTER(!BOUND(?webDeweyClass))
      }
    `
  },
  {
    id: 'example-5',
    title: 'Kvalifikatorer etter antall strenger',
    query: `
      PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
      PREFIX bs: <https://schema.bs.no/>

      SELECT
        ?kvalifikator
        ?termNb
        ?termNn
        (COUNT(?streng) AS ?antallStrenger)
      WHERE
      {
        ?kvalifikator a bs:Qualifier ;
                      skos:prefLabel ?termNb ;
                      skos:prefLabel ?termNn ;
                      skos:narrower ?streng .

        FILTER(lang(?termNb) = "nb")
        FILTER(lang(?termNn) = "nn")
      }
      GROUP BY ?kvalifikator ?termNb ?termNn
      ORDER BY DESC(?antallStrenger)
    `
  },
  {
    id: 'example-6',
    title: 'Entitetstyper og antall forekomster per type',
    query: `
      prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
      PREFIX bs: <https://schema.bs.no/>
      PREFIX bibbi: <https://id.bs.no/bibbi/>

      SELECT
        ?type
        (COUNT(?q) AS ?antall)
      WHERE
      {
        ?q a ?type
      }
      GROUP BY ?type
      ORDER BY DESC(?antall)
    `
  },
  {
    id: 'example-7',
    title: 'Antall dokumenter en person er tilknyttet',
    query: `
      PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
      PREFIX bs: <https://schema.bs.no/>
      PREFIX bibbi: <https://id.bs.no/bibbi/>

      SELECT 
        # Personen
        ?person 
        ?personEtikett 
        ?ansvarshavendeFor
        ?direkteEmneFor

        # Personen som emne
        (COUNT(?personEmne) AS ?emneStrenger)
        (SUM(?emneFor) AS ?emneForTotalt) 
      WHERE
      {
        # Personen (hovedinnførsel)
        ?person a bs:Person .   
        ?person skos:prefLabel ?personEtikett .
        FILTER(LANG(?personEtikett) = 'nb')
        ?person bs:itemsAsEntry ?ansvarshavendeFor .
        OPTIONAL { ?person bs:itemsAsSubject ?direkteEmneFor . }

        # Personen som emne (1 eller flere biinførsler)
        ?personEmne a bs:PersonSubject .
        ?personEmne skos:broader ?person .
        ?personEmne bs:itemsAsSubject ?emneFor .
      }
      GROUP BY ?person ?personEtikett ?ansvarshavendeFor ?direkteEmneFor
      ORDER BY DESC(?emneForTotalt) DESC(?ansvarshavendeFor)
      # LIMIT 500
    `
  },
  {
    id: 'example-8',
    title: 'Titler brukt som emne, per person',
    query: `
      PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
      PREFIX bs: <https://schema.bs.no/>
      PREFIX bibbi: <https://id.bs.no/bibbi/>

      SELECT
        ?person
        ?personEtikett 
        (COUNT(?tittelEmne) AS ?titlerSomEmne)

      WHERE
      {
          # "Tittel som emne"-entiteten er enten hektet på en "Tittel"-entitet, hvis en slik finnes,
          # eller direkte på "Person"-entiteten, hvis ikke.
          # Derfor én eller to skos:broader for å komme fra ?tittelEmne til ?person
          ?tittelEmne a bs:TitleSubject ;
                      skos:broader/skos:broader? ?person .

          ?person a bs:Person ;
                  skos:prefLabel ?personEtikett .
          FILTER(LANG(?personEtikett) = 'nb')

      }
      GROUP BY ?person ?personEtikett 
    `
  },
  // {
  //   id: 'example-X',
  //   title: '',
  //   query: `
  //   `
  // },
];
