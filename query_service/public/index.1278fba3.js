(function () {
  function $parcel$interopDefault(a) {
    return a && a.__esModule ? a.default : a;
  }
  // ASSET: node_modules/dedent/dist/dedent.js
  var $0a99894e9361f35ccd8b033e3b0db82f$exports = {};
  function $0a99894e9361f35ccd8b033e3b0db82f$var$dedent(strings) {
    var raw = void 0;
    if (typeof strings === "string") {
      // dedent can be used as a plain function
      raw = [strings];
    } else {
      raw = strings.raw;
    }
    // first, perform interpolation
    var result = "";
    for (var i = 0; i < raw.length; i++) {
      result += raw[i].// join lines when there is a suppressed newline
      replace(/\\\n[ \t]*/g, "").// handle escaped backticks
      replace(/\\`/g, "`");
      if (i < (arguments.length <= 1 ? 0 : arguments.length - 1)) {
        result += arguments.length <= i + 1 ? undefined : arguments[i + 1];
      }
    }
    // now strip indentation
    var lines = result.split("\n");
    var mindent = null;
    lines.forEach(function (l) {
      var m = l.match(/^(\s+)\S+/);
      if (m) {
        var indent = m[1].length;
        if (!mindent) {
          // this is the first indented line
          mindent = indent;
        } else {
          mindent = Math.min(mindent, indent);
        }
      }
    });
    if (mindent !== null) {
      result = lines.map(function (l) {
        return l[0] === " " ? l.slice(mindent) : l;
      }).join("\n");
    }
    // dedent eats leading and trailing whitespace too
    result = result.trim();
    // handle escaped newlines at the end to ensure they don't get stripped too
    return result.replace(/\\n/g, "\n");
  }
  if ("object" !== "undefined") {
    $0a99894e9361f35ccd8b033e3b0db82f$exports = $0a99894e9361f35ccd8b033e3b0db82f$var$dedent;
  }
  var $0a99894e9361f35ccd8b033e3b0db82f$$interop$default = /*@__PURE__*/$parcel$interopDefault($0a99894e9361f35ccd8b033e3b0db82f$exports);
  var $1470947d5c54f15e5747dd368c672a8c$export$default = [{
    id: 'example-1',
    title: 'Oversikt over datasett (grafer)',
    query: `
      PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

      SELECT
        ?graph
        ?tripleCount
        ?conceptCount
      WHERE
      {
        {
          SELECT ?graph (COUNT(?s) AS ?tripleCount)
          WHERE {
            GRAPH ?graph { ?s ?p ?o }
          }
          GROUP BY ?graph
        }
        OPTIONAL
        {
          SELECT ?graph (COUNT(?c) AS ?conceptCount)
          WHERE {
            GRAPH ?graph { ?c a skos:Concept }
          }
          GROUP BY ?graph
        }
      }
    `
  }, {
    id: 'example-2',
    title: 'Bibbi-autoriteter etter bruksfrekvens',
    query: `
      PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
      PREFIX bs: <https://schema.bs.no/>
      PREFIX bibbi: <https://id.bs.no/bibbi/>
      
      SELECT
        ?concept
        ?conceptLabel
        ?itemCount
      WHERE
      {
        ?concept skos:inScheme bibbi: ;
                  bs:itemsAsSubject ?itemCount ;
                  skos:prefLabel ?conceptLabel .
        FILTER(lang(?conceptLabel) = "nb")
      }
      ORDER BY DESC(?itemCount)
    `
  }, {
    id: 'example-3',
    title: 'WebDewey-numre etter antall Bibbi-autoriteter',
    query: `
      PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
      PREFIX bs: <https://schema.bs.no/>
      PREFIX bibbi: <https://id.bs.no/bibbi/>
      
      SELECT
        ?webDeweyNr
        ?webDeweyClass
        ?webDeweyHeading
        (COUNT(?concept) AS ?conceptCount)
      WHERE
      {
        ?concept skos:inScheme bibbi: ;
                bs:webdewey ?webDeweyNrRaw .
      
        # Fjerner /
        BIND(REPLACE(?webDeweyNrRaw, "/", "") AS ?webDeweyNr)
      
        OPTIONAL {
          ?webDeweyClass skos:notation ?webDeweyNr ;
                        skos:prefLabel ?webDeweyHeading .
        }
      }
      GROUP BY ?webDeweyNr ?webDeweyClass ?webDeweyHeading
      ORDER BY DESC(?conceptCount)
    `
  }, {
    id: 'example-4',
    title: 'Bibbi-autoriteter med godkjente WebDewey-numre som ikke lar seg slå opp i Norsk WebDewey',
    query: `
      PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
      PREFIX bs: <https://schema.bs.no/>
      PREFIX bibbi: <https://id.bs.no/bibbi/>
      
      SELECT
        ?concept
        ?term
        ?itemCount
        ?webDeweyNumber
      WHERE
      {
      
        # Vi ønsker begreper som hører til vokabularet <https://id.bs.no/bibbi/> (Bibbi):
        ?concept skos:inScheme bibbi: .
      
        # I tillegg til URI-en ønsker vi foretrukket term (skos:prefLabel) og antall dokumenter (bs:itemCount)
        ?concept skos:prefLabel ?term .
        FILTER(LANG(?term) = "nb") # på bokmål
        ?concept bs:itemsAsSubject ?itemCount .
      
        # Vi ønsker bare begreper som har et (godkjent) WebDewey-nummer.
        # Fra egenskapen bs:webdewey får vi ut WebDewey-nummeret som streng, slik det står på posten:
        ?concept bs:webdewey ?webDeweyNumber .
      
        # Til slutt filtrerer vi på WebDewey-numre som *ikke* lar seg slå opp direkte i WebDewey
        # (typisk bygde numre som ikke er bygd i WebDewey)
        OPTIONAL {
          ?concept skos:closeMatch ?webDeweyClass .
          FILTER(STRSTARTS(STR(?webDeweyClass), "http://dewey.info"))
          ?webDeweyClass skos:inScheme <http://dewey.info/scheme/edition/e23/> .
        }
        FILTER(!BOUND(?webDeweyClass))
      }
    `
  }, {
    id: 'example-5',
    title: 'Kvalifikatorer etter antall strenger',
    query: `
      PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
      PREFIX bs: <https://schema.bs.no/>

      SELECT
        ?kvalifikator
        ?termNb
        ?termNn
        (COUNT(?streng) AS ?antallStrenger)
      WHERE
      {
        ?kvalifikator a bs:Qualifier ;
                      skos:prefLabel ?termNb ;
                      skos:prefLabel ?termNn ;
                      skos:narrower ?streng .

        FILTER(lang(?termNb) = "nb")
        FILTER(lang(?termNn) = "nn")
      }
      GROUP BY ?kvalifikator ?termNb ?termNn
      ORDER BY DESC(?antallStrenger)
    `
  }, {
    id: 'example-6',
    title: 'Entitetstyper og antall forekomster per type',
    query: `
      prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
      PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
      PREFIX bs: <https://schema.bs.no/>
      PREFIX bibbi: <https://id.bs.no/bibbi/>

      SELECT
        ?type
        (COUNT(?q) AS ?antall)
      WHERE
      {
        ?q a ?type
      }
      GROUP BY ?type
      ORDER BY DESC(?antall)
    `
  }, {
    id: 'example-7',
    title: 'Antall dokumenter en person er tilknyttet',
    query: `
      PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
      PREFIX bs: <https://schema.bs.no/>
      PREFIX bibbi: <https://id.bs.no/bibbi/>

      SELECT 
        # Personen
        ?person 
        ?personEtikett 
        ?ansvarshavendeFor
        ?direkteEmneFor

        # Personen som emne
        (COUNT(?personEmne) AS ?emneStrenger)
        (SUM(?emneFor) AS ?emneForTotalt) 
      WHERE
      {
        # Personen (hovedinnførsel)
        ?person a bs:Person .   
        ?person skos:prefLabel ?personEtikett .
        FILTER(LANG(?personEtikett) = 'nb')
        ?person bs:itemsAsEntry ?ansvarshavendeFor .
        OPTIONAL { ?person bs:itemsAsSubject ?direkteEmneFor . }

        # Personen som emne (1 eller flere biinførsler)
        ?personEmne a bs:PersonSubject .
        ?personEmne skos:broader ?person .
        ?personEmne bs:itemsAsSubject ?emneFor .
      }
      GROUP BY ?person ?personEtikett ?ansvarshavendeFor ?direkteEmneFor
      ORDER BY DESC(?emneForTotalt) DESC(?ansvarshavendeFor)
      # LIMIT 500
    `
  }, {
    id: 'example-8',
    title: 'Titler brukt som emne, per person',
    query: `
      PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
      PREFIX bs: <https://schema.bs.no/>
      PREFIX bibbi: <https://id.bs.no/bibbi/>

      SELECT
        ?person
        ?personEtikett 
        (COUNT(?tittelEmne) AS ?titlerSomEmne)

      WHERE
      {
          # "Tittel som emne"-entiteten er enten hektet på en "Tittel"-entitet, hvis en slik finnes,
          # eller direkte på "Person"-entiteten, hvis ikke.
          # Derfor én eller to skos:broader for å komme fra ?tittelEmne til ?person
          ?tittelEmne a bs:TitleSubject ;
                      skos:broader/skos:broader? ?person .

          ?person a bs:Person ;
                  skos:prefLabel ?personEtikett .
          FILTER(LANG(?personEtikett) = 'nb')

      }
      GROUP BY ?person ?personEtikett 
    `
    // {
    // id: 'example-X',
    // title: '',
    // query: `
    // `
    // },
  }];
  // -----------------------------------------------------------------------------
  const $5a0fc18a3d05ed062c9c85241c2e04ed$var$defaultValue = `PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX bibbi: <https://id.bs.no/>
PREFIX bs: <https://schema.bs.no/>

SELECT ?entity ?entityLabel WHERE {
  ?entity a bs:Place ;
          skos:prefLabel ?entityLabel .

  FILTER(LANG(?entityLabel) = 'nb')
}

ORDER BY ASC(?entityLabel)
LIMIT 10`;
  // -----------------------------------------------------------------------------
  // Add prefix autocompleter
  const $5a0fc18a3d05ed062c9c85241c2e04ed$var$prefixList = [// Vocabularies we use
  'skos: <http://www.w3.org/2004/02/skos/core#>', 'isothes: <http://purl.org/iso25964/skos-thes#>', 'rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>', 'rdfs: <http://www.w3.org/2000/01/rdf-schema#>', 'dct: <http://purl.org/dc/terms/>', // Vocabularies for linked entities
  'wd: <http://www.wikidata.org/entity/>', 'wdt: <http://www.wikidata.org/prop/direct/>', // Our own vocabulary
  'bibbi: <https://id.bs.no/>', 'bs: <https://schema.bs.no/>'];
  YASQE.Autocompleters._prefixes = YASQE.Autocompleters.prefixes;
  YASQE.Autocompleters.prefixes = function (yasqe, completerName) {
    var completer = YASQE.Autocompleters._prefixes(yasqe, completerName);
    completer.async = false;
    completer.get = $5a0fc18a3d05ed062c9c85241c2e04ed$var$prefixList;
    return completer;
  };
  // -----------------------------------------------------------------------------
  // Add class autocompleter
  YASQE.Autocompleters._classes = YASQE.Autocompleters.classes;
  YASQE.Autocompleters.classes = function (yasqe, completerName) {
    var completer = YASQE.Autocompleters._classes(yasqe, completerName);
    let _get = completer.get.bind(completer);
    completer.get = function (token, callback) {
      if (!token || !token.string || token.string.trim().length == 0) {
        yasqe.autocompleters.notifications.getEl(completer).empty().append("Nothing to autocomplete yet!");
        return false;
      }
      if (token.autocompletionString.startsWith('https://schema.bs.no/')) {
        callback(["https://schema.bs.no/Topic", "https://schema.bs.no/Entity", "https://schema.bs.no/EntityCandidate", "https://schema.bs.no/Place", "https://schema.bs.no/Corporation", "https://schema.bs.no/Qualifier"]);
        return;
      }
      _get(token, function (res, n2) {
        console.log('Got results', res);
        callback(res);
      });
    };
    return completer;
  };
  // -----------------------------------------------------------------------------
  var $5a0fc18a3d05ed062c9c85241c2e04ed$var$yasqe = YASQE(document.getElementById('yasqe'), {
    value: $5a0fc18a3d05ed062c9c85241c2e04ed$var$defaultValue,
    sparql: {
      showQueryButton: true,
      endpoint: undefined,
      requestMethod: "POST"
    },
    viewportMargin: Infinity
  });
  var $5a0fc18a3d05ed062c9c85241c2e04ed$var$yasr = YASR(document.getElementById('yasr'), {
    getUsedPrefixes: $5a0fc18a3d05ed062c9c85241c2e04ed$var$yasqe.getPrefixesFromQuery
  });
  // link both together
  $5a0fc18a3d05ed062c9c85241c2e04ed$var$yasqe.options.sparql.callbacks.complete = $5a0fc18a3d05ed062c9c85241c2e04ed$var$yasr.setResponse;
  /*if (!yasr.somethingDrawn()) {
  yasqe.query();
  }*/
  $1470947d5c54f15e5747dd368c672a8c$export$default.forEach(example => {
    $('#examples').append(`<option value="${example.id}">${example.title}</option>`);
  });
  $('#examples').on('change', function () {
    console.log(this.value);
    if (this.value == '') return;
    const example = $1470947d5c54f15e5747dd368c672a8c$export$default.filter(ex => ex.id == this.value);
    if (example.length) {
      $5a0fc18a3d05ed062c9c85241c2e04ed$var$yasqe.setValue($0a99894e9361f35ccd8b033e3b0db82f$$interop$default(example[0].query));
    }
  });
})();

