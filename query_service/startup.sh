#!/bin/bash

set -euo pipefail

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "Starting query_service using endpoint: $SPARQL_ENDPOINT"

sed -i -e "s@{{SPARQL_ENDPOINT}}@$SPARQL_ENDPOINT@g" /usr/share/nginx/html/app.js

echo "OK!"

exec "/docker-entrypoint.sh $@"
