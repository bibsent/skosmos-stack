# Skosmos-stack

## Development environment

Start by cloning the repository:

    $ git clone --recursive https://gitlab.com/bibsent/skosmos-stack.git

(or do `git submodule init && git submodule update` if you forget the recursive flag).

And use `docker-compose` to start the services:

    $ docker-compose up -d

This will expose Fuseki at http://localhost:8001 , Skosmos at http://localhost:8002 and a SPARQL query interface at http://localhost:8003

Note: If you want to override environment variables without modifying `docker-compose.yml`,
you can create [an `.env` file](https://docs.docker.com/compose/environment-variables/).

## Loading data

1. Add some data to the `./staging` folder. For example the latest data from the prod server:

    scp lds-skosmos-prod01:/tmp/skosmos-staging-data/*.nt ./staging/

2. Load the data into the TDB2 database `/fuseki/databases/ds`. Fuseki must be stopped when using tdbloader:

    $ docker-compose stop fuseki
    $ docker-compose run jena tdb2.tdbloader --loc=/fuseki/databases/ds --graph=https://graph.bs.no/bibbi /staging/bibbi.nt
    $ docker-compose run fuseki java -cp /jena-fuseki/fuseki-server.jar jena.textindexer --desc=/jena-fuseki/config.ttl
    $ docker-compose start fuseki

For the data to be visible in Skosmos, the graph name must match one of the graphs in `./skosmos/config.ttl`.

## Tips

- SPARQL queries issued by Skosmos can be found both from `docker-compose logs fuseki` and in the browser console.
- `fuseki_cache` in an in-memory Varnish instance between Skosmos and Fuseki.
  To clear it, just restart it: `docker-compose restart fuseki_cache`.

## Timeouts

There are multiple timeouts that can affect the service:

- `fuseki/config.ttl`: `arq:queryTimeout` defines for how long Fuseki will attempt to carry out a query before returning a 500 response "ERROR: HTTP request for SPARQL query failed".
- `fuseki_cache/varnish.vcl`: `first_byte_timeout` defines for how long Varnish will wait for a response from Fuseki.
- `skosmos/config.ttl`: `skosmos:sparqlTimeout` defines for how long EasyRDF (PHP library used by Skosmos) will wait for a response from Varnish.

