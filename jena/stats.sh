#!/bin/sh

QUERY=$(cat <<'EOF'
SELECT (COUNT(*) AS ?nt) WHERE { ?s ?p ?o . }
EOF
)

if ping -q -c 1 fuseki > /dev/null 2>&1 ; then
	echo "Querying Fuseki SPARQL endpoint"
	RESULTS="$(echo "${QUERY}" | rsparql --service=http://fuseki:3030/ds --query=- --results json)"
else
	echo "Querying TDB"
	RESULTS="$(echo "${QUERY}" | tdb2.tdbquery --loc=/fuseki/databases/ds --query=- --results json)"
fi

COUNT="$(echo "$RESULTS" | jq -r '.results.bindings[0].nt.value')"

echo "[stats] RDF store contains $COUNT triples"